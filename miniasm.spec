Name:    miniasm
Version: 0.3
Release: 3
Summary: Ultrafast de novo assembly for long noisy reads
License: MIT
URL:	 https://github.com/lh3/miniasm
Source0: https://github.com/lh3/miniasm/archive/v%{version}.tar.gz#/%{name}-%{version}.tar.gz

BuildRequires: 	gcc zlib-devel

%description
Miniasm is a very fast OLC-based de novo assembler for noisy long reads. It takes all-vs-all read self-mappings (typically by minimap) as input and outputs an assembly graph in the GFA format. Different from mainstream assemblers, miniasm does not have a consensus step. It simply concatenates pieces of read sequences to generate the final unitig sequences. Thus the per-base error rate is similar to the raw input reads.

%prep
%setup -q -n %{name}-%{version}/

%build
%make_build

%install
install -d %{buildroot}/%{_bindir}
install -d %{buildroot}/%{_mandir}/man1
install -m 755 miniasm %{buildroot}/%{_bindir}
install -m 755 minidot %{buildroot}/%{_bindir}
install -m 644 miniasm.1 %{buildroot}/%{_mandir}/man1/

%pre
%preun
%post
%postun

%check

%files
%license LICENSE.txt
%doc README.md
%{_bindir}/*
%{_mandir}/*

%changelog
* Wed Aug 24 2022 wangkai <wangkai385@h-partners.com> - 0.3-3
- Enable debuginfo for fix strip

* Tue Oct 13 2020 Qingqing Li <liqingqing3@huawei.com> - 0.3-2
- update source0

* Sun Mar 29 2020 Wei Xiong <myeuler@163.com> - 0.3-1
- Package init

